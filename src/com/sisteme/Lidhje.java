/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sisteme;

/**
 *
 * @author Pavllo
 */
class Lidhje {
    private String input;
    private GjendjaView gjendje;

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public GjendjaView getGjendje() {
        return gjendje;
    }

    public void setGjendje(GjendjaView gjendje) {
        this.gjendje = gjendje;
    }

    @Override
    public boolean equals(Object obj) {
   Lidhje l=(Lidhje)obj;
   return input==l.getInput()&&gjendje.getNr()==l.getGjendje().getNr();
    }
    
}
