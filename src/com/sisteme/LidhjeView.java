/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sisteme;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D;
import javax.swing.JComponent;

/**
 *
 * @author Pavllo
 */
public class LidhjeView extends JComponent{
    private int x1,x2,y1,y2;
    private Color color=Color.black;
   String input;
     
    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    public LidhjeView(int x1, int y1,int x2,  int y2,String input) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
         this.input=input;
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }
    
    
    @Override
	public void paintComponent(Graphics g)
    {
        Graphics2D g2=(Graphics2D) g;
         g2.setColor(color);
        g2.drawLine(x1, y1, x2,  y2);
   if(input.contains(",") )   g2.drawString(input, (x1+x2)/2-20, (y1+y2)/2);
   else g2.drawString(input, (x1+x2)/2-10, (y1+y2)/2);
   koka(g2,new Point(x2,y2),new Point(x1,y1));

    }
  
    private void koka(Graphics2D g2, Point tip, Point tail)
    {
     double   phi = Math.toRadians(40);
       int barb = 10;
        double dy = tip.y - tail.y;
        double dx = tip.x - tail.x;
        double theta = Math.atan2(dy, dx);
        //System.out.println("theta = " + Math.toDegrees(theta));
        double x, y, rho = theta + phi;
        for(int j = 0; j < 2; j++)
        {
            x = tip.x - barb * Math.cos(rho);
            y = tip.y - barb * Math.sin(rho);
            g2.draw(new Line2D.Double(tip.x, tip.y, x, y));
            rho = theta - phi;
        }
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

}