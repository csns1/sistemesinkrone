package com.sisteme;

import com.alee.laf.WebLookAndFeel;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class SistemeSinkrone extends JFrame {

    public SistemeSinkrone() {

        this.setTitle("Sekuenca");
        this.setSize(325, 300);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(3);
        GridLayout gl = new GridLayout(4, 1);
        JLabel label = new JLabel("Vendos Sekuencen");

        JTextField tf = new JTextField(10);
        this.getContentPane().setLayout(gl);
        this.getContentPane().add(label);
        this.getContentPane().add(tf);
        JLabel error = new JLabel();
        JButton btn = new JButton("Zbato Algoritmin");
        btn.addActionListener((ActionEvent event) -> {
            if (tf.getText().length() > 0) {
                if (tf.getText().matches("[01]+")) {
                    new MainClass(tf.getText()).setVisible(true);
                    dispose();
                }

            }
            error.setText("Sekuenca e Gabuar Vendosi Nje Sekuence Binare");
            error.setForeground(Color.red);
            revalidate();
            repaint();
        });
        this.getContentPane().add(btn);
        this.getContentPane().add(error);

    }

    public static void main(String[] args) {

        WebLookAndFeel.install(true);
        WebLookAndFeel.setDecorateAllWindows(true);
        new SistemeSinkrone().setVisible(true);

    }

}
