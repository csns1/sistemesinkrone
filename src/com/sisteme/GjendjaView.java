/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sisteme;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import javax.swing.JComponent;

/**
 *
 * @author Pavllo
 */
public class GjendjaView extends JComponent {
    private int nr;
    private int x;
    private int y;
    private Color color = Color.black;
    ArrayList<Lidhje> lidhjet = new ArrayList<>();
    public GjendjaView() {
        nr = -1;
        x = 0;
        y = 0;
    }

    public GjendjaView(int a, int x, int y) {
        nr = a;
        this.x = x;
        this.y = y;
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(color);
        if (color.equals(Color.GREEN)) {
            g2.fill(new Ellipse2D.Double(x, y, 50, 50));
        }
        g2.setColor(Color.BLACK);
        g2.drawOval(x, y, 50, 50);
    }

    public boolean contains(Point p) {
        if (p.getX() > x && p.getX() < x + 50 && p.getY() > y && p.getY() < y + 50) {
            return true;
        }

        return false;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getX1() {
        return x;
    }

    public void setX1(int x) {
        this.x = x;
    }

    public int getY1() {
        return y;
    }

    public void setY1(int y) {
        this.y = y;
    }

    public ArrayList<Lidhje> getLidhjet() {
        return lidhjet;
    }

    public void setLidhjet(ArrayList<Lidhje> lidhjet) {
        this.lidhjet = lidhjet;
    }

    public void shtoLidhje(Lidhje lidhje) {
        this.lidhjet.add(lidhje);
    }

}
