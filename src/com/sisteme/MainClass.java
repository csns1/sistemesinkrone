/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sisteme;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Pavllo
 */
@SuppressWarnings("serial")
public class MainClass extends JFrame implements MouseListener {

    private JPanel navigation = new JPanel(new GridLayout(8, 1));
    private String sekuenca;
    private ArrayList<GjendjaView> allGjendjet = new ArrayList<>();

    private GjendjaView selected = null;

    public MainClass(String sekuenca) {
        this.setTitle("Diagrama Sekuencore");
        this.setVisible(true);
        this.setDefaultCloseOperation(3);
        this.setSize(1500, 1000);
        this.sekuenca = sekuenca;
        krijoNvaigation();
        algoritmi();
        this.getContentPane().addMouseListener(this);
        this.setLocationRelativeTo(null);
        this.setExtendedState(this.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        this.repaint();
        this.revalidate();

    }

    private void algoritmi() {
        String[] seq = sekuenca.split("(?!^)");
        GjendjaView gjendjaParaardhese = new GjendjaView(0, 0, 80);
        GjendjaView gjendjaPasardhese = new GjendjaView();
        for (int i = 0; i < seq.length; i++) {
            String string = seq[i];
            gjendjaPasardhese = new GjendjaView(i + 1, (i + 1) * 70, i % 2 * 70 + 20);
            Lidhje lidhje = new Lidhje();
            lidhje.setGjendje(gjendjaPasardhese);
            if (i != seq.length - 1) {
                lidhje.setInput(string + "/0");
            } else {
                lidhje.setInput(string + "/1");
            }
            gjendjaParaardhese.shtoLidhje(lidhje);
            allGjendjet.add(gjendjaParaardhese);
            gjendjaParaardhese = gjendjaPasardhese;
        }
        allGjendjet.add(gjendjaPasardhese);
        newgjendje();
        lidhjet();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (selected == null) {
            for (GjendjaView gjendjaView : allGjendjet) {
                if (gjendjaView.contains(e.getPoint())) {
                    selected = gjendjaView;
                    gjendjaView.setColor(Color.GREEN);
                    this.revalidate();
                    this.repaint();
                }
            }
        } else {
            selected.setX1(e.getX());
            selected.setY1(e.getY());
            selected.setColor(Color.BLACK);
            this.getContentPane().removeAll();
            this.getContentPane().add(navigation, BorderLayout.SOUTH);
            newgjendje();
            lidhjeNew();
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    private void krijoNvaigation() {

        navigation.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10),
                BorderFactory.createMatteBorder(2, 0, 0, 0, Color.BLACK)));
        Dimension d = new Dimension();
        d.setSize(this.getWidth(), 80);
        navigation.setPreferredSize(d);
        navigation.setLayout(new GridLayout(1, 8));
        JButton tabela = new JButton("Tabela");
        tabela.addActionListener(e -> {
            afishoTabelen();
        });
        JButton reset = new JButton("Reset");
        reset.addActionListener(e -> {

            clearAll();

        });

        JButton gjendjet = new JButton("Gjendjet");
        gjendjet.addActionListener(e -> {
            this.getContentPane().removeAll();
            this.getContentPane().add(navigation, BorderLayout.SOUTH);
            newgjendje();
            lidhjeNew();
        });
        navigation.add(gjendjet);
        navigation.add(tabela);
        navigation.add(reset);

        navigation.add(new JPanel());
        navigation.add(new JPanel());
        navigation.add(new JPanel());
        this.getContentPane().add(navigation, BorderLayout.SOUTH);
    }

    private void afishoTabelen() {
        JTable table;
        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        ArrayList<String> tableHead = new ArrayList<>();
        tableHead.add("Gjendjet");
        String bitet = "";
        double logaritmiBaz2 = (Math.log(allGjendjet.size()) / Math.log(2));
        int numriBitevePerGjendje;
        if (logaritmiBaz2 % 2 == 0.0) {
            numriBitevePerGjendje = (int) logaritmiBaz2;
        } else {
            numriBitevePerGjendje = (int) logaritmiBaz2;
            numriBitevePerGjendje++;
        }
        for (int i = 0; i < numriBitevePerGjendje; i++) {
            bitet += "x" + i;
        }
        tableHead.add(bitet);
        tableHead.add("y0");
        tableHead.add("y1");
        String output0 = "";
        String output1 = "";
        for (int i = 0; i < numriBitevePerGjendje; i++) {
            output0 += alphabet[i] + "0";
            output1 += alphabet[i] + "1";
        }

        tableHead.add(output0);
        tableHead.add(output1);
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, tableHead.toArray(new String[tableHead.size()]));
        int[][] greyTable = greyTable(allGjendjet.size());
        ArrayList<String> rreshti;
        for (Integer i = 0; i < allGjendjet.size(); i++) {
            rreshti = new ArrayList<>();
            rreshti.add(i.toString());
            rreshti.add(Arrays.toString(greyTable[i]).replace(",", "").replace("[", "").replace("]", ""));
            Integer y0, y1;
            if (allGjendjet.get(i).getLidhjet().get(0).getInput().startsWith("0")) {
                y0 = allGjendjet.get(i).getLidhjet().get(0).getGjendje().getNr();
                y1 = allGjendjet.get(i).getLidhjet().get(1).getGjendje().getNr();
            } else {
                y0 = allGjendjet.get(i).getLidhjet().get(1).getGjendje().getNr();
                y1 = allGjendjet.get(i).getLidhjet().get(0).getGjendje().getNr();
            }
            rreshti.add(y0.toString());

            rreshti.add(y1.toString());
            rreshti.add(Arrays.toString(greyTable[y0]).replace(",", "").replace("[", "").replace("]", ""));
            rreshti.add(Arrays.toString(greyTable[y1]).replace(",", "").replace("[", "").replace("]", ""));
            model.addRow(rreshti.toArray());
        }
        table = new JTable(model);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
        table.setDefaultRenderer(String.class, centerRenderer);

        this.getContentPane().removeAll();
        this.getContentPane().add(new JScrollPane(table), BorderLayout.CENTER);
        this.add(navigation, BorderLayout.SOUTH);
        this.getContentPane().repaint();
        this.getContentPane().revalidate();

    }

    protected void newgjendje() {
        for (GjendjaView gjendjaView : allGjendjet) {
            this.getContentPane().add(gjendjaView);
            this.revalidate();
            this.repaint();
        }

    }

    protected void clearAll() {
        new SistemeSinkrone().setVisible(true);

        this.dispose();
    }

    private Point coordinatesChild(GjendjaView gjendje1, GjendjaView gjendjaView2) {
        Point p = new Point();
        if (gjendjaView2.getY1() >= gjendje1.getY1()) {
            p.y = gjendjaView2.getY1();
        } else {
            p.y = gjendjaView2.getY1() + 50;
        }
        p.x = gjendjaView2.getX1() + 25;
        return p;
    }

    private Point coordinatesParent(GjendjaView gjendje1, GjendjaView gjendjaView2) {
        Point p = new Point();
        if (gjendje1.getX1() >= gjendjaView2.getX1()) {
            p.x = gjendje1.getX1();
        } else {
            p.x = gjendje1.getX1() + 50;
        }
        p.y = gjendje1.getY1() + 25;
        return p;
    }

    private void lidhjeNew() {

        for (GjendjaView gjendjaView : allGjendjet) {
            ArrayList<Lidhje> lidhjet = gjendjaView.getLidhjet();
            ArrayList<LidhjeView> twoLidhjet = new ArrayList<>();
            for (Lidhje lidhje : lidhjet) {
                Point p1 = coordinatesParent(gjendjaView, lidhje.getGjendje());
                Point p2 = coordinatesChild(gjendjaView, lidhje.getGjendje());
                LidhjeView l = new LidhjeView(p1.x, p1.y, p2.x, p2.y, lidhje.getInput());
                twoLidhjet.add(l);

            }
            if (lidhjet.get(0).getGjendje().getNr() == lidhjet.get(1).getGjendje().getNr()) {
                String s = twoLidhjet.get(0).getInput();
                s += ",";
                s += twoLidhjet.get(1).getInput();
                LidhjeView l = twoLidhjet.get(0);
                l.setInput(s);
                this.getContentPane().add(l);
                this.revalidate();
                this.repaint();
            } else {
                for (LidhjeView lidhjeView : twoLidhjet) {
                    this.getContentPane().add(lidhjeView);
                    this.revalidate();
                    this.repaint();

                }
            }

        }
        selected = null;
    }

    private void lidhjet() {
        String[] split = sekuenca.split("");
        if (split[0].equals("0")) {
            shtoNje(0);

        } else {
            shtoZero(0);
        }
        for (int i = 1; i < allGjendjet.size() - 1; i++) {
            if (split[i].equals("0")) {
                shtoNje(i);
            } else {
                shtoZero(i);
            }
        }
        shtoZero(allGjendjet.size() - 1);
        shtoNje(allGjendjet.size() - 1);
        lidhjeNew();
    }

    private void shtoNje(int index) {
        String[] split = sekuenca.split("(?!^)");
        String s;
        int poz;
        int c = 0;
        String str = "";
        if (index != 1) {
            str += "1";
        }
        if (index == 1 && split[0].equals("0")) {
            str += "1";
        }
        int x = -1;
        while (c != index) {
            s = split[index - 1 - c];
            str = s + str;
            poz = sekuenca.indexOf(str);
            if (poz == -1) {
                break;
            } else if (poz == 0) {
                x = poz + str.length();
            } 
             c++;
        }
        Lidhje l1 = new Lidhje();
        if (x == -1 && split[0].equals("1")) {
            x = 1;
        }
        if (x == -1) {
            x = 0;
        }
        l1.setGjendje(allGjendjet.get(x));
        if (x == allGjendjet.size() - 1) {
            l1.setInput("1/1");
        } else {
            l1.setInput("1/0");
        }
        allGjendjet.get(index).shtoLidhje(l1);
    }

    private void shtoZero(int index) {
        String[] split = sekuenca.split("(?!^)");
        String s = split[split.length - 1];
        int poz = 0;
        int c = 0;
        String str = "";
        if (index != 1) {
            str += "0";
        }
        if (index == 1 && split[0].equals("1")) {
            str += "0";
        }
        int x = -1;
        while (c != index) {
            s = split[index - 1 - c];
            str = s + str;

            poz = sekuenca.indexOf(str);
            if (poz == -1) {
                break;
            } else if (poz == 0) {
                x = poz + str.length();
                c++;
            } else {
                c++;
            }

        }

        Lidhje l1 = new Lidhje();
        if (x == -1 && split[0].equals("0")) {
            x = 1;
        }
        if (x == -1) {
            x = 0;
        }

        l1.setGjendje(allGjendjet.get(x));
        if (x == allGjendjet.size() - 1) {
            l1.setInput("0/1");
        } else {
            l1.setInput("0/0");
        }
        allGjendjet.get(index).shtoLidhje(l1);
    }

    private int[][] greyTable(int numriIGjenjdeve) {
        //Logarimti Me baze 2 per te gjetur numrin e par me te vogel per t cilin 2 ne fuqi nr
        // te jet m i vogel se nr i gjenjeve
        int numriBiteve = (int) (Math.log(numriIGjenjdeve - 1) / Math.log(2));
        numriBiteve++;
        // 2 ne fuqi te numrit t biteve per t llogaritur madhesine e tabeles
//        int gjatesiaETabeles = (int) Math.pow(2, numriBiteve);
        int gjatesiaETabeles = numriIGjenjdeve;
        //inicializimi i tabeles
        int[][] tablaGrey = new int[gjatesiaETabeles][numriBiteve];
        //pas inicializimit e kemi t gjith tabelen t inicializuar me 0 
        //dhe per kodin grey dy numrat e par jan 0000 dhe 0001 numrin e par e kemi
        //kshu q bejme numrin e dyte 0001 
        tablaGrey[1][numriBiteve - 1] = 1;
        //numri i biteve qe do pasqyrohen si fillim eshte 1 pastaj 2 ..3..4 
        int numriIBiteveQeDoPasqyrohen = 1;
        //duke qen se kemi dy numrat e par n rregull bredhim tabelen
        //duke filluar nga poz i 3 pra indeksi 2
        for (int i = 2; i < gjatesiaETabeles; i++) {
            //numri i rrjestave
            int numriRrjeshtaveTeCilatDoPasqyrohen = (int) Math.pow(2, numriIBiteveQeDoPasqyrohen);
            for (int j = 0; j < numriRrjeshtaveTeCilatDoPasqyrohen; j++) {
                //nqs kemi kaluar madhesine e tabeles nuk vazhdojme te paqyrojme dhe nderpresim ciklin
                if (j + numriRrjeshtaveTeCilatDoPasqyrohen >= gjatesiaETabeles) {
                    break;
                }
                //pasqyrojme bitet duke u nisur nga indeksi i fundit
                for (int k = numriBiteve - 1; k >= numriBiteve - numriIBiteveQeDoPasqyrohen; k--) {
                    tablaGrey[numriRrjeshtaveTeCilatDoPasqyrohen + j][k] = tablaGrey[numriRrjeshtaveTeCilatDoPasqyrohen - j - 1][k];

                }
                //ndryshojme bitin para biteve te pasqyruar me 1
                tablaGrey[numriRrjeshtaveTeCilatDoPasqyrohen + j][numriBiteve - numriIBiteveQeDoPasqyrohen - 1] = 1;
            }
            //kalojme indeksin i tek rrjeshti i fundit i pasqyruar 
            i = 2 + numriRrjeshtaveTeCilatDoPasqyrohen - 1;
            numriIBiteveQeDoPasqyrohen++;

        }
        return tablaGrey;
    }

}
